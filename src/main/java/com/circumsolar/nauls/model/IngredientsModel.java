package com.circumsolar.nauls.model;

import com.circumsolar.nauls.db.DatabaseManager;
import com.circumsolar.nauls.db.Ingredient;
import org.apache.wicket.model.AbstractReadOnlyModel;

import java.sql.SQLException;
import java.util.List;

public class IngredientsModel extends AbstractReadOnlyModel<List<Ingredient>> {

    @Override
    public List<Ingredient> getObject() {

        try {

            return DatabaseManager.getInstance()
                    .getIngredientsDao()
                    .queryBuilder()
                    .orderBy(Ingredient.NAME_FIELD_NAME, true)
                    .query();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
