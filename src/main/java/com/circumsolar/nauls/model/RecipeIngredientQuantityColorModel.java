package com.circumsolar.nauls.model;

import com.circumsolar.nauls.db.DatabaseManager;
import com.circumsolar.nauls.db.FridgeIngredient;
import com.circumsolar.nauls.db.RecipeIngredient;
import org.apache.wicket.model.AbstractReadOnlyModel;

import java.util.List;

public class RecipeIngredientQuantityColorModel extends AbstractReadOnlyModel<String> {

    private RecipeIngredient recipeIngredient;

    public RecipeIngredientQuantityColorModel(RecipeIngredient recipeIngredient) {
        this.recipeIngredient = recipeIngredient;
    }

    @Override
    public String getObject() {

        List<FridgeIngredient> fridgeIngredients = DatabaseManager.getInstance().getFridgeIngredientsDao()
                .queryForEq(FridgeIngredient.INGREDIENT_ID_FIELD_NAME, recipeIngredient.getIngredient().getId());

        if (fridgeIngredients.size() == 0 ||
                fridgeIngredients.get(0).getQuantity() < recipeIngredient.getQuantity()) {

            return "color: red";
        }

        return "color: black";
    }
}
