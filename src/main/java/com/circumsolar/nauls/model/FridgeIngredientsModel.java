package com.circumsolar.nauls.model;

import com.circumsolar.nauls.db.DatabaseManager;
import com.circumsolar.nauls.db.FridgeIngredient;
import com.circumsolar.nauls.db.Ingredient;
import com.j256.ormlite.stmt.QueryBuilder;
import org.apache.wicket.model.AbstractReadOnlyModel;

import java.sql.SQLException;
import java.util.List;

public class FridgeIngredientsModel extends AbstractReadOnlyModel<List<FridgeIngredient>> {

    @Override
    public List<FridgeIngredient> getObject() {

        try {
            return query();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private List<FridgeIngredient> query() throws SQLException {

        QueryBuilder<FridgeIngredient, Long> fridgeIngredientsQuery = DatabaseManager.getInstance()
                .getFridgeIngredientsDao().queryBuilder();

        QueryBuilder<Ingredient, Long> ingredientsQuery = DatabaseManager.getInstance().getIngredientsDao()
                .queryBuilder();
        ingredientsQuery.orderBy(Ingredient.NAME_FIELD_NAME, true);

        List<FridgeIngredient> fridgeIngredients = fridgeIngredientsQuery.join(ingredientsQuery).query();

        for (FridgeIngredient fridgeIngredient : fridgeIngredients) {
            DatabaseManager.getInstance().getIngredientsDao().refresh(fridgeIngredient.getIngredient());
        }

        return fridgeIngredients;
    }
}
