package com.circumsolar.nauls.model;

import com.circumsolar.nauls.db.DatabaseManager;
import com.circumsolar.nauls.db.FridgeIngredient;
import com.circumsolar.nauls.db.RecipeIngredient;
import org.apache.wicket.model.AbstractReadOnlyModel;

import java.util.List;

public class RecipeColorModel extends AbstractReadOnlyModel<String> {

    private long recipeId;

    public RecipeColorModel(long recipeId) {
        this.recipeId = recipeId;
    }

    @Override
    public String getObject() {

        List<RecipeIngredient> recipeIngredients = DatabaseManager.getInstance().getRecipeIngredientsDao()
                .queryForEq(RecipeIngredient.RECIPE_ID_FIELD_NAME, recipeId);

        for (RecipeIngredient recipeIngredient : recipeIngredients) {

            List<FridgeIngredient> fridgeIngredients = DatabaseManager.getInstance().getFridgeIngredientsDao()
                    .queryForEq(FridgeIngredient.INGREDIENT_ID_FIELD_NAME, recipeIngredient.getIngredient().getId());

            if (fridgeIngredients.size() == 0 ||
                    fridgeIngredients.get(0).getQuantity() < recipeIngredient.getQuantity()) {
                return "color: red";
            }
        }

        return "color: black";
    }
}
