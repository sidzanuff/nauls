package com.circumsolar.nauls.model;

import com.circumsolar.nauls.db.DatabaseManager;
import com.circumsolar.nauls.db.Ingredient;
import com.circumsolar.nauls.db.RecipeIngredient;
import com.j256.ormlite.stmt.QueryBuilder;
import org.apache.wicket.model.AbstractReadOnlyModel;

import java.sql.SQLException;
import java.util.List;

public class RecipeIngredientsModel extends AbstractReadOnlyModel<List<RecipeIngredient>> {

    private long recipeId;

    public RecipeIngredientsModel(long recipeId) {
        this.recipeId = recipeId;
    }

    @Override
    public List<RecipeIngredient> getObject() {

        try {
            return query();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private List<RecipeIngredient> query() throws SQLException {

        QueryBuilder<RecipeIngredient, Long> recipeIngredientsQuery = DatabaseManager.getInstance()
                .getRecipeIngredientsDao().queryBuilder();
        recipeIngredientsQuery.where().eq(RecipeIngredient.RECIPE_ID_FIELD_NAME, recipeId).prepare();

        QueryBuilder<Ingredient, Long> ingredientsQuery = DatabaseManager.getInstance().getIngredientsDao()
                .queryBuilder();
        ingredientsQuery.orderBy(Ingredient.NAME_FIELD_NAME, true);

        List<RecipeIngredient> recipeIngredients = recipeIngredientsQuery.join(ingredientsQuery).query();

        for (RecipeIngredient recipeIngredient : recipeIngredients) {
            DatabaseManager.getInstance().getIngredientsDao().refresh(recipeIngredient.getIngredient());
        }

        return recipeIngredients;
    }
}
