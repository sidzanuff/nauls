package com.circumsolar.nauls.model;

import com.circumsolar.nauls.db.DatabaseManager;
import com.circumsolar.nauls.db.Recipe;
import org.apache.wicket.model.AbstractReadOnlyModel;

import java.sql.SQLException;
import java.util.List;

public class RecipesModel extends AbstractReadOnlyModel<List<Recipe>> {

    @Override
    public List<Recipe> getObject() {

        try {

            return DatabaseManager.getInstance()
                    .getRecipesDao()
                    .queryBuilder()
                    .orderBy(Recipe.NAME_FIELD_NAME, true)
                    .query();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
