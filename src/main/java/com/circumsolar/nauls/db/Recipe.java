package com.circumsolar.nauls.db;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

@SuppressWarnings("unused")
@DatabaseTable
public class Recipe {

    public static final String NAME_FIELD_NAME = "name";

    @DatabaseField(generatedId = true)
    private long id;
    @DatabaseField(columnName = NAME_FIELD_NAME)
    private String name;
    @ForeignCollectionField
    private ForeignCollection<RecipeIngredient> ingredients;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<RecipeIngredient> getIngredients() {
        return ingredients;
    }
}
