package com.circumsolar.nauls.db;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class FridgeIngredient {

    public static final String INGREDIENT_ID_FIELD_NAME = "ingredient_id";
    public static final String QUANTITY_FIELD_NAME = "quantity";

    @DatabaseField(generatedId = true)
    private long id;
    @DatabaseField(foreign = true, columnName = INGREDIENT_ID_FIELD_NAME)
    private Ingredient ingredient;
    @DatabaseField(columnName = QUANTITY_FIELD_NAME)
    private double quantity;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }
}
