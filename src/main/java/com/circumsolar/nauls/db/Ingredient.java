package com.circumsolar.nauls.db;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@SuppressWarnings("unused")
@DatabaseTable
public class Ingredient {

    public static final String NAME_FIELD_NAME = "name";

    @DatabaseField(generatedId = true)
    private Long id;
    @DatabaseField(columnName = NAME_FIELD_NAME)
    private String name;
    @DatabaseField
    private String unit;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name + " [" + unit + "]";
    }
}
