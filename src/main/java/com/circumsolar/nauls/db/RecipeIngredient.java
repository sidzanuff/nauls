package com.circumsolar.nauls.db;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@SuppressWarnings("unused")
@DatabaseTable
public class RecipeIngredient {

    public static final String RECIPE_ID_FIELD_NAME = "recipe_id";
    public static final String INGREDIENT_ID_FIELD_NAME = "ingredient_id";

    @DatabaseField(generatedId = true)
    private Long id;
    @DatabaseField(foreign = true, columnName = RECIPE_ID_FIELD_NAME)
    private Recipe recipe;
    @DatabaseField(foreign = true, columnName = INGREDIENT_ID_FIELD_NAME)
    private Ingredient ingredient;
    @DatabaseField
    private double quantity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }
}
