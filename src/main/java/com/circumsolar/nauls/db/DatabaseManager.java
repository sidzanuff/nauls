package com.circumsolar.nauls.db;

import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DatabaseManager {

    private static final String DATABASE_URL = "jdbc:h2:./nauls";

    private static DatabaseManager instance = new DatabaseManager();

    public static DatabaseManager getInstance() {
        return instance;
    }

    private RuntimeExceptionDao<Ingredient, Long> ingredientsDao;
    private RuntimeExceptionDao<RecipeIngredient, Long> recipeIngredientsDao;
    private RuntimeExceptionDao<Recipe, Long> recipesDao;
    private RuntimeExceptionDao<FridgeIngredient, Long> fridgeIngredientsDao;

    private DatabaseManager() {

        try {
            init();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void init() throws SQLException {

        JdbcPooledConnectionSource pooledConnectionSource = new JdbcPooledConnectionSource(DATABASE_URL);
        pooledConnectionSource.setMaxConnectionAgeMillis(5 * 60 * 1000);
        pooledConnectionSource.setCheckConnectionsEveryMillis(60 * 1000);
        pooledConnectionSource.setTestBeforeGet(true);

        TableUtils.createTableIfNotExists(pooledConnectionSource, Ingredient.class);
        TableUtils.createTableIfNotExists(pooledConnectionSource, RecipeIngredient.class);
        TableUtils.createTableIfNotExists(pooledConnectionSource, Recipe.class);
        TableUtils.createTableIfNotExists(pooledConnectionSource, FridgeIngredient.class);

        ingredientsDao = RuntimeExceptionDao.createDao(pooledConnectionSource, Ingredient.class);
        recipeIngredientsDao = RuntimeExceptionDao.createDao(pooledConnectionSource, RecipeIngredient.class);
        recipesDao = RuntimeExceptionDao.createDao(pooledConnectionSource, Recipe.class);
        fridgeIngredientsDao = RuntimeExceptionDao.createDao(pooledConnectionSource, FridgeIngredient.class);
    }

    public RuntimeExceptionDao<Ingredient, Long> getIngredientsDao() {
        return ingredientsDao;
    }

    public RuntimeExceptionDao<RecipeIngredient, Long> getRecipeIngredientsDao() {
        return recipeIngredientsDao;
    }

    public RuntimeExceptionDao<Recipe, Long> getRecipesDao() {
        return recipesDao;
    }

    public RuntimeExceptionDao<FridgeIngredient, Long> getFridgeIngredientsDao() {
        return fridgeIngredientsDao;
    }
}
