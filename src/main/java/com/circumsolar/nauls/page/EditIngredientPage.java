package com.circumsolar.nauls.page;

import com.circumsolar.nauls.component.IngredientForm;
import com.circumsolar.nauls.db.DatabaseManager;
import com.circumsolar.nauls.db.Ingredient;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class EditIngredientPage extends BasePage {

    public static final String PARAM_INGREDIENT_ID = "ingredientId";

    public EditIngredientPage(PageParameters parameters) {

        Long ingredientId = parameters.get(PARAM_INGREDIENT_ID).toLong();
        Ingredient ingredient = DatabaseManager.getInstance().getIngredientsDao().queryForId(ingredientId);

        add(new IngredientForm("form", ingredient) {
            @Override
            protected void onSubmit() {
                super.onSubmit();
                setResponsePage(IngredientListPage.class);
            }
        });
    }
}
