package com.circumsolar.nauls.page;

import org.apache.wicket.Application;
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class BasePage extends WebPage {

//    @Override
//    protected void onConfigure() {
//        super.onConfigure();
//
//        AuthenticatedWebApplication app = (AuthenticatedWebApplication) Application.get();
//
//
//        if (!AuthenticatedWebSession.get().isSignedIn()) {
//            app.restartResponseAtSignInPage();
//        }
//    }

    public BasePage() {
        init();
    }

    public BasePage(PageParameters parameters) {
        super(parameters);
        init();
    }

    private void init() {

//        add(new Link("logOut") {
//
//            @Override
//            public void onClick() {
//                AuthenticatedWebSession.get().invalidate();
//                setResponsePage(getApplication().getHomePage());
//            }
//        });
    }
}