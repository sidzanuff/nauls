package com.circumsolar.nauls.page;

import com.circumsolar.nauls.component.RecipeIngredientForm;
import com.circumsolar.nauls.component.RecipeNameForm;
import com.circumsolar.nauls.db.DatabaseManager;
import com.circumsolar.nauls.db.Recipe;
import com.circumsolar.nauls.db.RecipeIngredient;
import com.circumsolar.nauls.model.RecipeIngredientQuantityColorModel;
import com.circumsolar.nauls.model.RecipeIngredientsModel;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class EditRecipePage extends BasePage {

    public static final String PARAM_RECIPE_ID = "recipeId";

    public EditRecipePage(PageParameters parameters) {

        long recipeId = parameters.get(PARAM_RECIPE_ID).toLong();
        Recipe recipe = DatabaseManager.getInstance().getRecipesDao().queryForId(recipeId);

        add(new RecipeNameForm("recipeNameForm", recipe));
        add(new RecipeIngredientForm("ingredientForm", recipe));

        add(new ListView<RecipeIngredient>("ingredients", new RecipeIngredientsModel(recipe.getId())) {
            @Override
            protected void populateItem(final ListItem<RecipeIngredient> item) {

                Label quantityLabel = new Label("quantity", new PropertyModel(item.getModel(), "quantity"));
                quantityLabel.add(AttributeModifier.append("style",
                        new RecipeIngredientQuantityColorModel(item.getModelObject())));

                item.add(new Label("name", new PropertyModel(item.getModel(), "ingredient.name")));
                item.add(quantityLabel);
                item.add(new Label("unit", new PropertyModel(item.getModel(), "ingredient.unit")));

                item.add(new Link("delete") {
                    @Override
                    public void onClick() {
                        DatabaseManager.getInstance().getRecipeIngredientsDao().delete(item.getModelObject());
                        setResponsePage(EditRecipePage.class, getPageParameters());
                    }
                });
            }
        });
    }
}
