package com.circumsolar.nauls.page;

import com.circumsolar.nauls.component.IngredientForm;
import com.circumsolar.nauls.db.DatabaseManager;
import com.circumsolar.nauls.db.Ingredient;
import com.circumsolar.nauls.model.IngredientsModel;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class IngredientListPage extends BasePage {

    public IngredientListPage() {

        add(new ListView<Ingredient>("ingredients", new IngredientsModel()) {

            @Override
            protected void populateItem(final ListItem<Ingredient> item) {

                item.add(new Link("edit") {
                    @Override
                    public void onClick() {

                        PageParameters pageParameters = new PageParameters();
                        pageParameters.add(EditIngredientPage.PARAM_INGREDIENT_ID, item.getModelObject().getId());

                        setResponsePage(EditIngredientPage.class, pageParameters);
                    }
                }.add(new Label("name", new PropertyModel(item.getModel(), "name"))));

                item.add(new Label("unit", new PropertyModel(item.getModel(), "unit")));

                item.add(new Link("delete") {
                    @Override
                    public void onClick() {
                        DatabaseManager.getInstance().getIngredientsDao().delete(item.getModelObject());
                        setResponsePage(IngredientListPage.class);
                    }
                });
            }
        });

        add(new IngredientForm("form"));
    }
}
