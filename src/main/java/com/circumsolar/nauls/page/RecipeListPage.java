package com.circumsolar.nauls.page;

import com.circumsolar.nauls.component.RecipeNameForm;
import com.circumsolar.nauls.db.DatabaseManager;
import com.circumsolar.nauls.db.Recipe;
import com.circumsolar.nauls.model.RecipeColorModel;
import com.circumsolar.nauls.model.RecipesModel;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

@SuppressWarnings("unused")
public class RecipeListPage extends BasePage {

    public RecipeListPage() {

        add(new RecipeNameForm("recipeNameForm") {
            @Override
            protected void onSubmit() {
                super.onSubmit();
                openEditRecipePage(this.getRecipe());
            }
        });

        add(new ListView<Recipe>("recipes", new RecipesModel()) {
            @Override
            protected void populateItem(final ListItem<Recipe> item) {

                Label nameLabel = new Label("name", new PropertyModel(item.getModel(), "name"));
                nameLabel.add(AttributeModifier.append("style",
                        new RecipeColorModel(item.getModelObject().getId())));

                Link editLink = new Link("edit") {
                    @Override
                    public void onClick() {
                        openEditRecipePage(item.getModelObject());
                    }
                };
                editLink.add(nameLabel);
                item.add(editLink);

                item.add(new Link("delete") {
                    @Override
                    public void onClick() {
                        DatabaseManager.getInstance().getRecipeIngredientsDao().delete(item.getModelObject().getIngredients());
                        DatabaseManager.getInstance().getRecipesDao().delete(item.getModelObject());
                        setResponsePage(RecipeListPage.class);
                    }
                });
            }
        });
    }

    private void openEditRecipePage(Recipe recipe) {
        PageParameters pageParameters = new PageParameters();
        pageParameters.add(EditRecipePage.PARAM_RECIPE_ID, recipe.getId());
        setResponsePage(EditRecipePage.class, pageParameters);
    }
}
