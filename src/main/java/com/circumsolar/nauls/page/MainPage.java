package com.circumsolar.nauls.page;

import org.apache.wicket.markup.html.basic.Label;

public class MainPage extends BasePage {

    public MainPage() {
        super();

        add(new Label("label", "Hello"));
    }
}