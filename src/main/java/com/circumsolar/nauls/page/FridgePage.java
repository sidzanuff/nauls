package com.circumsolar.nauls.page;

import com.circumsolar.nauls.component.IngredientActionForm;
import com.circumsolar.nauls.db.FridgeIngredient;
import com.circumsolar.nauls.model.FridgeIngredientsModel;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.PropertyModel;

@SuppressWarnings("unused")
public class FridgePage extends BasePage {

    public FridgePage() {

        add(new IngredientActionForm("ingredientForm"));

        add(new ListView<FridgeIngredient>("ingredients", new FridgeIngredientsModel()) {
            @Override
            protected void populateItem(final ListItem<FridgeIngredient> item) {

                item.add(new Label("name", new PropertyModel(item.getModel(), "ingredient.name")));
                item.add(new Label("quantity", new PropertyModel(item.getModel(), "quantity")));
                item.add(new Label("unit", new PropertyModel(item.getModel(), "ingredient.unit")));
            }
        });
    }
}
