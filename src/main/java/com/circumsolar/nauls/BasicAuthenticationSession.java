package com.circumsolar.nauls;

import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.request.Request;

import java.security.MessageDigest;

public class BasicAuthenticationSession extends AuthenticatedWebSession {

    private static final String PASS = "b65e30e43adc2216b7d9de082e10c141ec18a7971d864705154ce3f8f1ffb513";

    public BasicAuthenticationSession(Request request) {
        super(request);
    }

    @Override
    public boolean authenticate(String username, String password) {

        return "admin".equals(username) && PASS.equals(sha256(password));
    }

    @Override
    public Roles getRoles() {
        return null;
    }

    public static String sha256(String base) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuilder hexString = new StringBuilder();

            for (byte aHash : hash) {
                String hex = Integer.toHexString(0xff & aHash);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}