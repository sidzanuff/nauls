package com.circumsolar.nauls.component;

import com.circumsolar.nauls.db.DatabaseManager;
import com.circumsolar.nauls.db.Recipe;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.PropertyModel;

public class RecipeNameForm extends Form {

    private Recipe recipe;

    public RecipeNameForm(String id) {
        super(id);
        init(new Recipe());
    }

    public RecipeNameForm(String id, Recipe recipe) {
        super(id);
        init(recipe);
    }

    public Recipe getRecipe() {
        return recipe;
    }

    private void init(Recipe recipe) {

        this.recipe = recipe;

        add(new TextField<>("recipeName", new PropertyModel<>(recipe, "name")));
    }

    @Override
    protected void onSubmit() {
        DatabaseManager.getInstance().getRecipesDao().createOrUpdate(recipe);
    }
}
