package com.circumsolar.nauls.component;

import com.circumsolar.nauls.db.DatabaseManager;
import com.circumsolar.nauls.db.Ingredient;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.PropertyModel;

public class IngredientForm extends Form {

    private Ingredient ingredient;

    public IngredientForm(String id) {
        super(id);
        init(new Ingredient());
    }

    public IngredientForm(String id, Ingredient ingredient) {
        super(id);
        init(ingredient);
    }

    private void init(Ingredient ingredient) {

        this.ingredient = ingredient;

        add(new TextField<>("ingredientName", new PropertyModel<>(this, "ingredient.name")));
        add(new TextField<>("ingredientUnit", new PropertyModel<>(this, "ingredient.unit")));
    }

    @Override
    protected void onSubmit() {
        DatabaseManager.getInstance().getIngredientsDao().createOrUpdate(ingredient);
        ingredient = new Ingredient();
    }
}
