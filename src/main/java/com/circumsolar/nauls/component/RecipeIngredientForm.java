package com.circumsolar.nauls.component;

import com.circumsolar.nauls.db.DatabaseManager;
import com.circumsolar.nauls.db.Ingredient;
import com.circumsolar.nauls.db.Recipe;
import com.circumsolar.nauls.db.RecipeIngredient;
import com.circumsolar.nauls.model.IngredientsModel;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.PropertyModel;

import java.sql.SQLException;
import java.util.List;

public class RecipeIngredientForm extends Form<RecipeIngredient> {

    private Recipe recipe;
    private RecipeIngredient recipeIngredient;

    public RecipeIngredientForm(String id, Recipe recipe) {
        super(id);

        this.recipe = recipe;

        createNewRecipeIngredient();

        add(new TextField<>("ingredientQuantity", new PropertyModel<Double>(this, "recipeIngredient.quantity")));

        add(new DropDownChoice<>("ingredient",
                new PropertyModel<Ingredient>(this, "recipeIngredient.ingredient"),
                new IngredientsModel()));
    }

    private void createNewRecipeIngredient() {

        recipeIngredient = new RecipeIngredient();
        recipeIngredient.setRecipe(recipe);
    }

    @Override
    protected void onSubmit() {

        if (recipeIngredient.getIngredient() == null || recipeIngredient.getQuantity() <= 0) {
            return;
        }

        List<RecipeIngredient> recipeIngredients;
        try {
            recipeIngredients = DatabaseManager.getInstance().getRecipeIngredientsDao()
                    .queryBuilder().where()
                    .eq(RecipeIngredient.RECIPE_ID_FIELD_NAME, recipe.getId())
                    .and()
                    .eq(RecipeIngredient.INGREDIENT_ID_FIELD_NAME, recipeIngredient.getIngredient().getId())
                    .query();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        if (recipeIngredients.size() > 0) {
            double quantity = recipeIngredient.getQuantity();
            recipeIngredient = recipeIngredients.get(0);
            recipeIngredient.setQuantity(recipeIngredient.getQuantity() + quantity);
        }

        DatabaseManager.getInstance().getRecipeIngredientsDao().createOrUpdate(recipeIngredient);
        createNewRecipeIngredient();
    }
}
