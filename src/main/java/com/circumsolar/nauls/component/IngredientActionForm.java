package com.circumsolar.nauls.component;

import com.circumsolar.nauls.db.DatabaseManager;
import com.circumsolar.nauls.db.FridgeIngredient;
import com.circumsolar.nauls.db.Ingredient;
import com.circumsolar.nauls.model.IngredientsModel;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.PropertyModel;

import java.util.List;

@SuppressWarnings("unused")
public class IngredientActionForm extends Form<Ingredient> {

    private Ingredient ingredient;
    private double quantity;

    public IngredientActionForm(String id) {
        super(id);

        add(new DropDownChoice<>("ingredient",
                new PropertyModel<Ingredient>(this, "ingredient"),
                new IngredientsModel()));

        add(new TextField<>("ingredientQuantity", new PropertyModel<Double>(this, "quantity")));
    }

    @Override
    protected void onSubmit() {

        if (ingredient == null) {
            return;
        }

        List<FridgeIngredient> fridgeIngredients = DatabaseManager.getInstance().getFridgeIngredientsDao()
                .queryForEq(FridgeIngredient.INGREDIENT_ID_FIELD_NAME, ingredient.getId());

        FridgeIngredient fridgeIngredient;
        if (fridgeIngredients.size() == 0) {
            fridgeIngredient = new FridgeIngredient();
            fridgeIngredient.setIngredient(ingredient);
        } else {
            fridgeIngredient = fridgeIngredients.get(0);
        }

        fridgeIngredient.setQuantity(fridgeIngredient.getQuantity() + quantity);

        if (fridgeIngredient.getQuantity() > 0) {
            DatabaseManager.getInstance().getFridgeIngredientsDao().createOrUpdate(fridgeIngredient);
        } else {
            DatabaseManager.getInstance().getFridgeIngredientsDao().delete(fridgeIngredient);
        }
    }
}
